from django.shortcuts import render, redirect
from .models import Student, Class
from .forms import CreateStudent
# Create your views here.

def home(request):
    return render(request, 'my_app/home.html', {})

def create(request):
    if request.method == 'POST':
        form = CreateStudent(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            student = Student(name = data['name'], dateOfBirth = data['dateOfBirth'], sex = data['sex'], currentClassId = Class.objects.get(id = int(data['currentClassId'])))
            student.save()
        return redirect('/home')
    else:
        form = CreateStudent()
    return render(request, 'my_app/create.html', {'form': form})

def list(request):
    class_list = Class.objects.all()
    return render(request, 'my_app/list.html', {'class_list': class_list})