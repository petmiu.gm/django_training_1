from django.db import models
from django.db.models.fields import CharField
from django.utils.translation import gettext_lazy
from datetime import date
# Create your models here.

class School(models.Model):
    name = models.CharField(max_length=100)
    class Grade(models.TextChoices):
        primary = 'PR', gettext_lazy('Primary School')
        secondary = 'SE', gettext_lazy('Secondary School')
        high = 'HI', gettext_lazy('High School')
    grade = models.CharField(max_length=2, choices=Grade.choices, default=Grade.primary)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'schools'
        verbose_name_plural = 'schools'

class Class(models.Model):
    name = models.CharField(max_length=100)
    Grade = models.CharField(max_length=2)
    schoolid = models.ForeignKey('School', on_delete=models.CASCADE)
    def __str__(self):
        return (self.Grade + '-' + self.name)
    class Meta:
        verbose_name = 'classes'
        verbose_name_plural = 'classes'

class Student(models.Model):
    name = models.CharField(max_length=100)
    dateOfBirth = models.DateField()
    class Sex(models.TextChoices):
        male = 'M', gettext_lazy('Male')
        female = 'F', gettext_lazy('Female')
    sex = models.CharField(max_length=1, choices=Sex.choices, default=Sex.male)
    currentClassId = models.ForeignKey('Class', on_delete=models.CASCADE, null=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'students'
        verbose_name_plural = 'students'
