from collections import UserList
from django.contrib import admin
from .models import *
from django import forms
# Register your models here.

# admin.site.register(School)
# admin.site.register(Class)
# admin.site.register(Student)

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('name', 'dateOfBirth', 'sex', 'currentClassId')
    list_filter = ('currentClassId', )
    search_fields = ('name__startswith', )
    empty_value_display = '---'
    # list_editable = ('sex', )
    list_display_links = ('name', 'currentClassId', )
    list_per_page = 8
    

@admin.register(School)
class SchoolAdmin(admin.ModelAdmin):
    list_display = ('name', 'grade', 'number_of_classes', )
    list_filter = ('grade', )
    search_fields = ('name__startswith', )
    def number_of_classes(self, obj):
        number_of_classes = len(School.objects.get(id = obj.id).class_set.all())
        return number_of_classes

@admin.register(Class)
class ClassAdmin(admin.ModelAdmin):
    list_display = ('name', 'Grade', 'schoolid', 'number_of_students')
    list_filter = ('Grade', 'schoolid')
    search_fields = ('name__startswith', )
    def number_of_students(self, obj):
        number_of_students = len(Class.objects.get(id = obj.id).student_set.all())
        return number_of_students

admin.site.site_header = 'My app'
admin.site.site_title = 'Admin'