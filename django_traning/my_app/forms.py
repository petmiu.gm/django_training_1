from django import forms
from .models import Class
import json
from .models import Student

class CreateStudent(forms.Form):
    name = forms.CharField(label = 'Name', max_length=100, required=True)
    dateOfBirth = forms.DateField(label = 'Birthday', required=True)
    sex_choices = [
        (Student.Sex.male, 'M'),
        (Student.Sex.female, 'F'),
    ]
    sex = forms.ChoiceField(label = 'Sex', choices=sex_choices, required=True, widget=forms.Select)
    class_list = Class.objects.all()
    class_choices = []

    for i in class_list:
        class_choices.append((i.id, i.Grade + '-' + i.name))
    currentClassId = forms.ChoiceField(label = 'Class', choices=class_choices, required=True, widget=forms.Select)
    class_choices = json.dumps(class_choices)